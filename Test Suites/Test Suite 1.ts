<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Test Suite 1</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>3</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>true</rerunImmediately>
   <testSuiteGuid>173c9e6a-541f-4d85-acf6-85368c1807a3</testSuiteGuid>
   <testCaseLink>
      <guid>6bd29b4c-42ec-46e1-bbfe-49042da99690</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Change password success</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>32e10507-8175-4edd-8611-5282dd858eeb</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Failed to change password with a length less than 8</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1ce00201-6670-489e-b363-65ccafb76129</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login Fail</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>fce29395-e757-499f-8496-048fe7e49120</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login Success</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a20187a2-6ac9-4a30-9984-28f0c5d1b7d5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Logout Success</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a7a615ff-3ca7-422b-9074-43b9c4e9ca7f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Not found characters</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2510e399-87c3-4bf0-afac-c3c5818c0f31</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Not found number</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e2a7968b-51ca-49b6-a13a-684c00148861</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Not found symbol</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
